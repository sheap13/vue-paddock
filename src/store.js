import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
  character: {
    name: 'Adventurer',
    defined: false,
    pos: [5, 5],
  },
  map: [],
  monsters: [
    {
      health: 5,
      pos: [2, 2],
    },
  ],
}

export const mutations = {
  setCharacter (state, character) {
    state.character = character
  },
  move (state, vector) {
    const newPos = [
      state.character.pos[0] + vector[0],
      state.character.pos[1] + vector[1],
    ]
    if (state.map[newPos[0]][newPos[1]] === '.') {
      state.character.pos = newPos
    }
  },
  setMap (state, map) {
    state.map = map
  },
}

export default new Vuex.Store({
  state,
  mutations,
})
