import Vue from 'vue'
import Router from 'vue-router'
import PaddockMaster from '@/components/paddock-master/PaddockMaster'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'PaddockMaster',
      component: PaddockMaster
    }
  ]
})
