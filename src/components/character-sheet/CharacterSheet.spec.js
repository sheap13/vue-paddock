import Vue from 'vue'
import { mount } from '@vue/test-utils'
import CharacterSheet from '@/components/character-sheet/CharacterSheet'

describe('CharacterSheet.vue', () => {
  const Constructor = Vue.extend(CharacterSheet)

  it('should render name field with defualt', () => {
    const vm = new Constructor().$mount()
    expect(vm.$el.querySelector('#char-name').value)
      .toEqual('Adventurer')
  })

  it('should have a start button', () => {
    const vm = new Constructor().$mount()
    expect(vm.$el.querySelector('#char-start').textContent)
      .toEqual('Start Game')
  })

  // it('should emit the defined character', () => {
  //   const wrapper = mount(CharacterSheet)
  //   wrapper.vm.handleStartClick() // TODO consider going via clicking button...
  //   expect(wrapper.emitted().character[0][0]).toEqual({
  //     name: 'Adventurer',
  //     defined: true,
  //     pos: [5, 5],
  //   })
  // })
})
