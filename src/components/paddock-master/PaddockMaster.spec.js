import Vue from 'vue'
import PaddockMaster from '@/components/paddock-master/PaddockMaster'

describe('PaddockMaster.vue', () => {
  let Constructor
  let vm

  beforeEach(() => {
    Constructor = Vue.extend(PaddockMaster)
    vm = new Constructor().$mount()
  })

  it('should render correct contents', () => {
    expect(vm.$el.querySelector('.hello h1').textContent)
      .toEqual('Welcome to Paddock Master')
  })
})
